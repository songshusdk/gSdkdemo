package gSdkdemo

import (
	"fmt"
	"testing"
	"time"
)

func TestGoWithRecovery(t *testing.T) {
	params := 1
	GoWithRecovery(func() {
		fmt.Println(params)
		panic("need be recovery")
	})
	time.Sleep(2 * time.Second)
}
