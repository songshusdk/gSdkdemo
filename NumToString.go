package gSdkdemo

import (
	"math"
	"strings"
)

/**
 * @Author: chenguiyun@songshuai.com - cgyjob
 * @Description:
 * @File:  NumToString.go
 * @Version: 1.0.0
 * @Date: 2022/4/10 9:56 下午
 */

var numToChar = "3pqr4su9vwx7klm8no6ybz0act1def2g5hij"

/*
10进制数转换   n 表示进制， 16 or 36
*/
func NumToBHex(num int, n int) string {
	numStr := ""
	for num != 0 {
		yu := num % n
		numStr = string(numToChar[yu]) + numStr
		num = num / n
	}
	return strings.ToUpper(numStr)
}

/*
36进制数转换  n 表示进制， 16 or 36
*/
func BHex2Num(str string, n int) int {
	str = strings.ToLower(str)
	v := 0.0
	length := len(str)
	for i := 0; i < length; i++ {
		s := string(str[i])
		index := strings.Index(numToChar, s)
		v += float64(index) * math.Pow(float64(n), float64(length-1-i)) // 倒序
	}
	return int(v)
}
