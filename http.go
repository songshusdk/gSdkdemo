package gSdkdemo

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"time"
)

// ----------------------------------------
//
//	全局默认的 HTTP 客户端
//
// ----------------------------------------
var gHTTPClient = CreateHTTPClient()

// 获取全局默认的 HTTP 客户端
func GetHTTPClient() *HTTPClient {
	return gHTTPClient
}

// ----------------------------------------
//
//	HTTP 客户端封装
//
// ----------------------------------------
type HTTPClient struct {
	client *http.Client
}

// 创建一个 HTTP 客户端
func CreateHTTPClient() *HTTPClient {
	return CreateHTTPClientWithClient(&http.Client{
		Timeout: time.Second * 30,
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		},
	})
}

// 从指定的底层客户端创建一个 HTTP 客户端
func CreateHTTPClientWithClient(client *http.Client) *HTTPClient {
	return &HTTPClient{client: client}
}

// 获取底层客户端
func (client *HTTPClient) Client() *http.Client {
	return client.client
}

// 发送一个自定义请求并获取结果
func (client *HTTPClient) Request(request *http.Request) ([]byte, error) {
	response, err := client.client.Do(request)
	if err != nil {
		return nil, err
	}

	defer func() {
		if response != nil && response.Body != nil {
			_ = response.Body.Close()
		}
	}()

	if response.StatusCode != http.StatusOK {
		return nil, errors.New(response.Status)
	}

	return ioutil.ReadAll(response.Body)
}

// 发送一个 GET 请求并获取结果
func (client *HTTPClient) Get(uri string) ([]byte, error) {
	if request, err := http.NewRequest(http.MethodGet, uri, nil); err == nil {
		return client.Request(request)
	} else {
		return nil, err
	}
}

// 发送一个 POST 请求并获取结果
func (client *HTTPClient) PostForm(uri string, values url.Values) ([]byte, error) {
	if request, err := http.NewRequest(http.MethodPost, uri, strings.NewReader(values.Encode())); err == nil {
		request.Header.Set("Content-Type", "application/x-www-form-urlencoded")
		return client.Request(request)
	} else {
		return nil, err
	}
}

// 发送一个 POST 请求并获取结果（发送 JSON 数据）
func (client *HTTPClient) PostJSON(uri string, values interface{}) ([]byte, error) {
	var body []byte

	switch values := values.(type) {
	case string:
		body = bytes.NewBufferString(values).Bytes()
	case []byte:
		body = values
	case *bytes.Buffer:
		body = values.Bytes()
	default:
		if bs, err := json.Marshal(values); err != nil {
			return nil, err
		} else {
			body = bs
		}
	}
	if request, err := http.NewRequest(http.MethodPost, uri, bytes.NewReader(body)); err == nil {
		request.Header.Set("Content-Type", "application/json")
		return client.Request(request)
	} else {
		return nil, err
	}
}

func (client *HTTPClient) PostJSONWithHeader(uri string, values interface{}, headerParams map[string]string) ([]byte, error) {
	var body []byte

	switch values := values.(type) {
	case string:
		body = bytes.NewBufferString(values).Bytes()
	case []byte:
		body = values
	case *bytes.Buffer:
		body = values.Bytes()
	default:
		if bs, err := json.Marshal(values); err != nil {
			return nil, err
		} else {
			body = bs
		}
	}
	if request, err := http.NewRequest(http.MethodPost, uri, bytes.NewReader(body)); err == nil {
		request.Header.Set("Content-Type", "application/json")
		if len(headerParams) != 0 {
			for headerKey, val := range headerParams {
				request.Header.Set(headerKey, val)
			}
		}
		return client.Request(request)
	} else {
		return nil, err
	}
}
