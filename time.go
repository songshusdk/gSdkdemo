package gSdkdemo

import "time"

const TimeLayout = "2006-01-02 15:04:05"
const YMDHIS = "2006-01-02 15:04:05"
const YMDHI = "2006-01-02 15:04"
const YMD = "2006-01-02"
const YM = "2006-01"
const Y = "2006"
const DayLayout = "2006-01-02"
const MonthLayout = "2006-01"
const UtcTimeLayout = "2006-01-02T15:04:05+08:00"
const CNMonthTimeLayout = "2006年01月"
const CNDayTimeLayout = "2006年01月02日"
const CNSimpleDayTimeLayout = "01月02日"
const YMDLayout = "20060102"
const YMLayout = "200601"
const YearLayout = "2006年"
const DayDotLayout = "2006.01.02"

const SecondDay = 86400

var LocalTime, _ = time.LoadLocation("Asia/Shanghai")

func StrToTime(timeStr string) time.Time {
	todayZero, _ := time.ParseInLocation(TimeLayout, timeStr, time.Local)
	return todayZero
}

func FormateTime(formate string) string {
	return time.Now().Format(formate)
}
