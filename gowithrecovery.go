package gSdkdemo

import (
	"github.com/zeromicro/go-zero/core/logx"
)

/*
 *  @Description:  针对不重要逻辑抛弃panic，如日志，打点，等不重要操作
 */
func GoWithRecovery(f func()) {
	go func() {
		defer func() {
			if err := recover(); err != nil {
				logx.Errorf("recover-panic err:%v", err)
			}
		}()
		f()
	}()
}
