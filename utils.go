package gSdkdemo

import (
	"crypto/md5"
	"fmt"
	"math/rand"
	"net"
	"os"
	"time"
)

func Sha1Hash(msg string) (hashData [16]byte) {
	return md5.Sum([]byte(msg))
}

func GetMd5(msg string) string {
	return fmt.Sprintf("%x", md5.Sum([]byte(msg)))
}

// 格式化用户名
func FormatUserNameStar(name string) (result string) {
	nameRune := []rune(name)
	lens := len(nameRune)

	if lens <= 1 {
		result = "***"
	} else if lens == 2 {
		result = string(nameRune[:1]) + "*"
	} else if lens == 3 {
		result = string(nameRune[:1]) + "*" + string(nameRune[2:3])
	} else if lens == 4 {
		result = string(nameRune[:1]) + "**" + string(nameRune[lens-1:lens])
	} else if lens > 4 {
		result = string(nameRune[:2]) + "***" + string(nameRune[lens-2:lens])
	}
	return
}

func InInt64Arr(findID int64, arr []int64) bool {
	for _, val := range arr {
		if findID == val {
			return true
		}
	}
	return false
}

func GetRandId() string {
	timestamp := time.Now().UnixNano() / 1e6
	return fmt.Sprintf("%d%d", timestamp, rand.Intn(99999))
}

func GetIpAddr() string {
	addrs, err1 := net.InterfaceAddrs()

	if err1 != nil {
		//fmt.Println(err1)
		os.Exit(1)
	}

	for _, address := range addrs {
		// 检查ip地址判断是否回环地址
		if ipnet, ok := address.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {

			if ipnet.IP.To4() != nil {
				return ipnet.IP.String()
			}
		}
	}
	return ""
}

/*
*
并集，map2覆盖map1
*/
func MapMerge(map1, map2 map[string]interface{}) map[string]interface{} {
	if len(map2) == 0 {
		return map1
	}
	for key, val := range map2 {
		map1[key] = val
	}
	return map1
}
